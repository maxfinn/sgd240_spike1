# Spike Report

## Setting up the Development Environment

### Introduction

This class uses Unreal Engine, C++, and git (using SourceTree, and the GitFlow method).
Therefore, it is required that we know how to set up the environment to support later work.

### Goals

1. A git repository, which contains:
    * A proper gitignore file for Unreal Engine as the first commit
    * The Unreal Engine First Person Shooter C++ project, without Starter Materials

### Personnel

* Primary - Max Finn
* Secondary - N/A

### Technologies, Tools, and Resources used

* [Learning Git](https://www.atlassian.com/git)
* [Get Started with SourceTree](https://confluence.atlassian.com/get-started-with-sourcetree/get-started-with-sourcetree-847359026.html)
* [Getting Started in Unreal Engine 4](https://docs.unrealengine.com/latest/INT/GettingStarted/)

### Tasks Undertaken

1. Followed Learning Git guide for source control
    1. Setup Bitbucket Account
    1. Download/Install SourceTree
    1. Create a new repository on Bitbucket
1. Followed Unreal Engine 
    1. Download/Install Unreal Engine
    1. Create a new project in Unreal Engine
1. Link with source control
    1. Temporarily append any character (eg. "A") to the UE4 project folder
    1. Via the Bitbucket website, clone the repository to the UE4 project's original location (sans the "A")
    1. Return the UE4 project folder to its original name, merging it with the cloned repository
    1. Commit the project changes back to Bitbucket via SourceTree

### What we found out

While some might consider the installation and base setup of modern software to be a rather simple process, I personally have found this particular collection of software to be instead quite difficult to setup as described. Account setup processes for Bitbucket and Unreal Engine are straightforward, but the issue arrises in linking the two together.

Unreal Engine requires you to create new projects in empty directories, as does cloning a pre-existing repository from Bitbucket through the GUI. We can seemingly use the command line to force a repository to apply over another directory, but in an example of using the path of least resistance, I have opted to use the previously outlined method of folder renaming instead.

![SourceTree landing page][SourceTree]

### Open Issues/Risks

1. Linking through SourceTree
    * Though not a true issue, over the course of spikes 1 and 2 I have yet to perform the linking process of the local repo with the Bitbucket repo inside of SourceTree (which I presume is the desired workflow).
1. Source control within Unreal Engine
    * Unreal Engine does appear to have built in source control, but I've yet to find the way to have that link with SourceTree, and potentially the Bitbucket account as well.
1. Safe editing of README.md
    * Having README.md stored in the repo arguably makes it safe, but I am uncertain as to whether there is any autosave process occurring during an individual online edit. Making a commit also seems larger than simply saving, so committing at the same rate I would traditionally hit save isn't ideal.

[SourceTree]: https://monosnap.com/file/hquzKRBexWCMAZtzYwfDLHtcYehPoo.png "SourceTree landing page"