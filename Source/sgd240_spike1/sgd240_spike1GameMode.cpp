// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#include "sgd240_spike1.h"
#include "sgd240_spike1GameMode.h"
#include "sgd240_spike1HUD.h"
#include "sgd240_spike1Character.h"

Asgd240_spike1GameMode::Asgd240_spike1GameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = Asgd240_spike1HUD::StaticClass();
}
